CREATE DATABASE IF NOT EXISTS `tracker`;
USE `tracker`;

CREATE TABLE IF NOT EXISTS `tracker_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(48) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `access_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

