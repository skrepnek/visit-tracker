const trackerAPI = 'http://localhost/visit-tracker/api';
const cookieExpireTimeMins = 1;

function registerVisit() {
  // Check if UUID cookie is present. If not generate one
  let trackerUUID = getCookie('tracker_uuid');
  if (trackerUUID === '') {
    trackerUUID = generateUUID();
  }
  // Set or refresh cookie
  setCookie('tracker_uuid', trackerUUID, cookieExpireTimeMins); // Ideally at least 60 minutes, for testing 1 minute

  // Call the API to register the current URL for this user
  const postObj = {
    uuid: trackerUUID,
    hostname: window.location.hostname,
    pathname: window.location.pathname,
    timestamp: new Date().getTime(),
  }
  const postBody = JSON.stringify(postObj)

  fetch(trackerAPI + '/register-tracker.php', {
    method: 'post',
    body: postBody,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost'
    }
  }).then((response) => {
    return response.json()
  }).then((res) => {
    console.log(res);
    if (res.status === 201) {
      console.log("Visit registered!")
    }
  }).catch((error) => {
    console.log(error)
  })
}

/*************************/
/*  Auxiliary Functions  */
/*************************/

// Generates a random UUID based on time from load and current time. There is still chance of collision.
// According to caniuse.com, crypto.randomUUID is supported on 73% of the browsers, hence not used here
function generateUUID() { // Public Domain/MIT
  let d = new Date().getTime();//Timestamp
  let d2 = ((typeof performance !== 'undefined') && performance.now && (performance.now() * 1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    let r = Math.random() * 16;//random number between 0 and 16
    if (d > 0) {//Use timestamp until depleted
      r = (d + r) % 16 | 0;
      d = Math.floor(d / 16);
    } else {//Use microseconds since page-load if supported
      r = (d2 + r) % 16 | 0;
      d2 = Math.floor(d2 / 16);
    }
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
}

function setCookie(name, value, minutes) {
  const d = new Date();
  d.setTime(d.getTime() + (minutes * 60 * 1000));
  let expires = "expires=" + d.toUTCString();
  document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

function getCookie(cookieName) {
  let name = cookieName + "=";
  let ca = document.cookie.split(';');
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

// Execution of script
registerVisit();
