# How does it work?

The `tracker.js` included in any standard page will send, on load, data to the API which will register the page view. A
cookie is set to determine a single user visiting a page in order to differentiate Total and Unique visits/views.

The cookie is set to expire in 1 minute for testing purposes. In reality, it's expected to be from 10 minutes to 2
hours, depending on the application.

## Steps to make it work

1. Create the Database with the `/db/schema.sql` provided
2. Update the `Database.php` constructor variables to make the Schema and Table created accessible
3. Publish the API on your webservice
    1. On local environment (e.g. localhost) it is usually published internally
    2. Note: CORS are enabled for all origins. Please be careful when publishing the resources in the web
4. Update the `/tracker/tracker.js` constants:
    1. `trackerAPI`: needs to match the API published
    2. `cookieExpireTimeMins`: desired time to expire
5. Publish the `tracker.js` file (it will be imported by other pages)
6. From here you can opt to use the test files on `/page-test/` or follow the items 7 and 8 for customized test
7. Import the `tracker.js` file on a page you want to track using script tag:
    1. `<script src="url/to/tracker.js"></script>`
8. Access the page tracked. New data should be available in the DB
9. Update the `trackerAPI` constant on `dashboard/index.html`
10. Access the Dashboard page and see the results

# The components

## Tracker

The single file `tracker.js` can be included in any standard page which you want to track visits

Ideally this file should be served in a CDN.

## DB

The `schema.sql` file have the structure to create the database and required tables for tracking

Simply run it on you RDBM

## API

It contains two endpoints files:

* `get-metrics.php`: Retrieve the metrics. It accepts the following get parameters:
    * `dateFrom` (Optional): Start date for the interval.
    * `dateTo` (Optional): End date for the interval
* `register-tracker.php`: Register a visit to a page. It accepts a POST request on JSON following this format:
  ```json
    {
      "uuid": "12345",
      "hostname": "hostname.of.page",
      "pathname": "path/name/to/page",
      "timestamp": "1651490367610"
    }
  ```

The `Database.php` is a class used to configure the DB access by extending PDO. Upon deployment on a server, the
parameters on the constructor must be updated to match your DB parameters.

Also, upon having the API service up, the tracker.js must be updated to reach the API correctly (`const trackerAPI`)

## Dashboard

This is the client side user interface. It's simple and allow the input of two optional dates (from and to) in order to
retrieve the metrics. The default brings the last 7 days of data on load

## Page Test

This is a simple 3-page test to test and verify accuracy of the tracker. Nothing special to note

# Project Scope

## In scope

* Track of standard pages
* Register all views
* Provide two metrics: total visits and unique visits per page
* README file
* DB Schema

## Not in scope

* Track on SPA pages
* Performance for high traffic and/or huge amount of data
* Extensive API (including security upgrades like token base access)
* Use of non-collision UUID script
* Exceptions
* Logs
* Unit tests

## Nice to have

- [x] Split JS and CSS from main html of dashboard - **DONE**
- [x] API response protocol matching status - **DONE**
- [ ] Full OOP API
    - [ ] Use of a route reader (based on regex)
    - [ ] Requests go through a Controller
    - [ ] DB variables read from .env file
- [ ] Better UI/UX design of the dashboard
    - [ ] Design for Desktop, Tablet and Mobile
- [ ] Tracker loaded on defer and reading user customized variables
- [ ] Bounce metrics
- [ ] Graphs on metrics