<?php

include_once "Database.php";

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');
header("Access-Control-Allow-Headers: X-Requested-With");

function getMetrics($dateFrom, $dateTo)
{
    $dateFilter = [];
    if (!is_null($dateFrom)) {
        $dateFromFormatted = date('Y-m-d H:i:s', $dateFrom);
        $dateFilter[] = 'access_time >= :dateFrom';
    }
    if (!is_null($dateTo)) {
        $dateToFormatted = date('Y-m-d H:i:s', $dateTo);
        $dateFilter[] = 'access_time <= :dateTo';
    }

    $statementFilter = '';
    if ($dateFilter) {
        $statementFilter = 'WHERE ' . implode(' AND ', $dateFilter);
    }

    $db = new \api\Database();
    $statement = $db->prepare(
        "SELECT td_grouped.url, COUNT(td_grouped.url) AS uniqueViews, SUM(totalVisits) AS totalViews FROM
            (SELECT url, COUNT(id) AS totalVisits
            FROM tracker_data
            {$statementFilter}
            GROUP BY uuid, url) AS td_grouped
        GROUP BY td_grouped.url
        ORDER BY uniqueViews DESC"
    );
    if (!empty($dateFilter)) {
        if (!is_null($dateFrom)) {
            $statement->bindParam(':dateFrom', $dateFromFormatted);
        }
        if (!is_null($dateTo)) {
            $statement->bindParam(':dateTo', $dateToFormatted);
        }
    }
    if (!$statement->execute()) {
        http_response_code(501);
        return ['status' => 501];
    }

    $data = [];
    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
    $data['visits'] = $result;
    $data['summary']['totalViews'] = array_sum(
        array_map(function ($item) {
            return $item['totalViews'];
        }, $result)
    );
    $data['summary']['uniqueViews'] = array_sum(
        array_map(function ($item) {
            return $item['uniqueViews'];
        }, $result)
    );


    return ['status' => 200, 'data' => $data];
}

echo json_encode(getMetrics($_GET['dateFrom'] ?? null, $_GET['dateTo'] ?? null));
