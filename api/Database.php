<?php

namespace api;

class Database extends \PDO
{
    public function __construct()
    {
        // This can be upgraded to read and retrieve data from a .env or .ini file for example
        $engine = 'mysql';
        $host = 'localhost';
        $dbname = 'tracker';
        $user = 'root';
        $password = '';

        parent::__construct($engine . ':host=' . $host . ';dbname=' . $dbname, $user, $password);
    }
}