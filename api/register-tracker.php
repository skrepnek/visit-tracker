<?php

include_once "Database.php";

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header("Access-Control-Allow-Headers: X-Requested-With");

function registerTracker()
{
    $postData = json_decode(file_get_contents('php://input'), true);

    if (!isset($postData['uuid']) || !isset($postData['hostname']) || !isset($postData['pathname']) || !isset($postData['timestamp'])) {
        http_response_code(400);
        return ['status' => 400];
    }
    $postData['url'] = $postData['hostname'] . $postData['pathname'];
    $postData['access_time'] = date('Y-m-d H:i:s', (int)$postData['timestamp'] / 1000);

    $db = new \api\Database();
    $statement = $db->prepare(
        "INSERT INTO tracker_data (uuid, url, access_time) VALUES (:uuid, :url, :access_time)"
    );
    $statement->bindParam(':uuid', $postData['uuid']);
    $statement->bindParam(':url', $postData['url']);
    $statement->bindParam(':access_time', $postData['access_time']);
    if ($statement->execute()) {
        http_response_code(201);
        return ['status' => 201];
    }

    http_response_code(500);
    return ['status' => 500];
}

echo json_encode(registerTracker());
