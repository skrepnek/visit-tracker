const trackerAPI = 'http://localhost/visit-tracker/api';

function updateMetrics() {
  clearMessages();
  clearResults();

  let trackerMetricsAPI = trackerAPI + '/get-metrics.php?';
  let requestParams = {};

  // Validate dates
  const dateFrom = document.querySelector('.date-from');
  if (dateFrom.value !== '') {
    if (!isValidDateString(dateFrom.value)) {
      displayError('Date "From" is invalid.', dateFrom);
    } else {
      requestParams.dateFrom = (new Date(dateFrom.value).getTime()) / 1000 | 0;
    }
  }

  const dateTo = document.querySelector('.date-to');
  if (dateTo.value !== '') {
    if (!isValidDateString(dateTo.value)) {
      displayError('Date "To" is invalid.', dateTo);
    } else {
      requestParams.dateTo = (new Date(dateTo.value).getTime()) / 1000 | 0;
    }
  }

  if (document.querySelectorAll('.error').length > 0) {
    return;
  }

  if (requestParams.dateFrom && requestParams.datedateTo && requestParams.dateFrom > requestParams.dateTo) {
    displayError('Date "From" can\'t be after date "To"', [dateFrom, dateTo]);
    return;
  }

  const queryString = Object.keys(requestParams)
    .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(requestParams[k]))
    .join('&');
  trackerMetricsAPI += queryString;

  fetch(trackerMetricsAPI, {
    method: 'get',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    }
  }).then((response) => {
    return response.json()
  }).then((res) => {
    if (res.status === 200) {
      drawSummary(res.data.summary);
      if (res.data.visits.length > 0) {
        drawVisitsTable(res.data.visits);
      } else {
        displayWarning("No data found on the selected interval");
      }
    }
  }).catch((error) => {
    console.log(error);
  })
}

function drawSummary(summaryData) {
  const summary = document.querySelector('.summary');

  const title = document.createElement('h1');
  title.appendChild(document.createTextNode('Summary'));

  const totalViews = document.createElement('p');
  totalViews.appendChild(document.createTextNode('Total Views: '));
  totalViews.appendChild(document.createTextNode(summaryData.totalViews));

  const uniqueViews = document.createElement('p');
  uniqueViews.appendChild(document.createTextNode('Unique Views: '));
  uniqueViews.appendChild(document.createTextNode(summaryData.uniqueViews));

  summary.append(title, totalViews, uniqueViews);
}

function drawVisitsTable(visits) {
  const tableContainer = document.querySelector('.table');

  // Create main table components
  const table = document.createElement('table');
  const thead = document.createElement('thead');
  const tbody = document.createElement('tbody');

  const title = document.createElement('h1');
  title.appendChild(document.createTextNode('Detailed views'));

  const dataHeader = Object.keys(visits[0]);

  // Create header row
  const theadRow = document.createElement('tr');
  dataHeader.forEach((header) => {
    const cell = document.createElement('th');
    cell.appendChild(document.createTextNode(camelCaseToTitleCaseText(header)));
    theadRow.appendChild(cell);
  });
  thead.appendChild(theadRow);

  // Create body rows
  visits.forEach(visit => {
    const tbodyRow = document.createElement('tr');
    dataHeader.forEach((header) => {
      const cell = document.createElement('td');
      cell.appendChild(document.createTextNode(visit[header]));
      if (header !== 'url') {
        cell.classList.toggle('center', true);
      }
      tbodyRow.appendChild(cell);
    });
    tbody.appendChild(tbodyRow);
  });

  // Append all elements to display
  table.appendChild(thead);
  table.appendChild(tbody);
  tableContainer.append(title);
  tableContainer.append(table);
}

function camelCaseToTitleCaseText(text) {
  const result = text.replace(/([A-Z])/g, ' $1');
  return result.charAt(0).toUpperCase() + result.slice(1);
}

function isValidDateString(dateString) {
  let date = new Date(dateString);
  return date instanceof Date && !isNaN(date.getTime());
}

function clearMessages() {
  const messageContainer = document.querySelector('.message');
  messageContainer.innerHTML = '';
  messageContainer.classList.toggle('hide', true);

  const errorElements = document.querySelectorAll('.error');
  for (let element of errorElements) {
    element.classList.toggle('error', false);
  }

  const warningElements = document.querySelectorAll('.warning');
  for (let element of warningElements) {
    element.classList.toggle('warning', false);
  }
}

function clearError(input) {
  input.classList.toggle('error', false);
}

function clearResults() {
  const summaryContainer = document.querySelector('.summary');
  const tableContainer = document.querySelector('.table');
  summaryContainer.innerHTML = '';
  tableContainer.innerHTML = '';
}

function displayError(message, input) {
  const messageContainer = document.querySelector('.message');
  messageContainer.appendChild(document.createTextNode(message));
  messageContainer.classList.toggle('error', true);
  messageContainer.classList.toggle('hide', false);

  if (Array.isArray(input)) {
    input.forEach((element) => element.classList.toggle('error', true));
  } else {
    input.classList.toggle('error', true);
  }
}

function displayWarning(message) {
  const messageContainer = document.querySelector('.message');
  messageContainer.appendChild(document.createTextNode(message));
  messageContainer.classList.toggle('warning', true);
  messageContainer.classList.toggle('hide', false);
}

window.onload = function () {
  document.querySelector('.date-from').value = new Date(
    new Date().setDate(new Date().getDate() - 7)
  ).toLocaleString("en-US");

  document.querySelector('.date-to').value = new Date().toLocaleString("en-US");

  updateMetrics();
}